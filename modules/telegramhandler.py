import logging
import requests
import sys


class TelegramHandler(logging.Handler):
    def __init__(self, bot_key, chat_id):
        logging.Handler.__init__(self)
        self.url = 'https://api.telegram.org/bot' + bot_key + '/sendMessage'
        self.chat_id = chat_id

    def emit(self, record):
        if record.module != 'connectionpool':
            requests.post(self.url, data={
                    'chat_id': self.chat_id,
                    'text': self.format(record),
                    'parse_mode': 'HTML'
                }
            )

        if record.levelno >= 40:
            sys.exit()
