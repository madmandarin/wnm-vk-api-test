import os
import logging
import logging.handlers
import modules.telegramhandler as telegramhandler


def create_log(debug, filename):
    # Настройка логов
    lg = logging.getLogger('')
    lg.setLevel(logging.DEBUG)

    # Логи в файл
    fh = logging.handlers.TimedRotatingFileHandler(
        filename + '.log',
        when='W0',
        interval=1,
        backupCount=0,
        encoding=None,
        delay=False,
        utc=False,
        atTime=None)
    formatter = logging.Formatter(fmt='[%(filename)s]%(levelname)-s:%(lineno)d[%(asctime)s]: %(message)s', datefmt=None,
                                  style='%')
    fh.setFormatter(formatter)
    if debug:
        fh.setLevel(logging.DEBUG)
    else:
        fh.setLevel(logging.WARNING)
    lg.addHandler(fh)

    # Вывод логов в консоль
    ch = logging.StreamHandler()
    formatter = logging.Formatter(fmt='[%(filename)s]%(levelname)-s:%(lineno)d[%(asctime)s]: %(message)s', datefmt=None,
                                  style='%')
    ch.setFormatter(formatter)
    if debug:
        ch.setLevel(logging.DEBUG)
    else:
        ch.setLevel(logging.WARNING)
    lg.addHandler(ch)

    # Отправка логов в телеграм
    if bool(os.getenv('WNM_LOG_TGBOT_TOKEN', False)) and bool(os.getenv('WNM_LOG_TGCHAT_ID', False)):
        lg.info("Turn on telegram handler")
        th = telegramhandler.TelegramHandler(
            bot_key=os.environ['WNM_LOG_TGBOT_TOKEN'],
            chat_id=int(os.environ['WNM_LOG_TGCHAT_ID'])
        )
        formatter = logging.Formatter(fmt='[%(filename)s]%(levelname)-s:%(lineno)d[%(asctime)s]: %(message)s', datefmt=None,
                                      style='%')
        th.setFormatter(formatter)
        th.setLevel(logging.ERROR)
        lg.addHandler(th)

    return lg
