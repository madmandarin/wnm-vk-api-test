import os
from peewee import *
from playhouse.db_url import connect

db = connect(os.environ.get('WNM_DB') or 'sqlite:///default.db')


class BaseModel(Model):
    class Meta:
        database = db


class Posts(BaseModel):
    date_update = DateTimeField()
    group_id = IntegerField()
    post_id = IntegerField()

    date_create = DateTimeField()
    views = IntegerField()
    likes = IntegerField()
    reposts = IntegerField()
    comments = IntegerField()
    votes = IntegerField()

    class Meta:
        indexes = (
            (('group_id', 'post_id', 'date_update'), True),
        )


with db:
    db.create_tables([Posts])
