import os
import requests

from datetime import date, datetime, timedelta
from flask import Flask, jsonify, request
from playhouse.shortcuts import model_to_dict

from modules import log
from modules.db import Posts


if not os.path.exists('logs'):
    os.makedirs('logs')
lg = log.create_log(debug=os.getenv('WNM_LOG_DEBUG', False), filename='logs/'+date.today().strftime("%Y-%m-%d"))


class VkApiException(Exception):
    pass


def get_vk_post(group_id, post_id):
    r = requests.get('https://api.vk.com/method/wall.getById', {
        'access_token': os.getenv('WNM_VK_TOKEN', ''),
        'posts': f'{group_id}_{post_id}',
        'version': 5.87
    })
    result = r.json()
    if 'response' in result and len(result["response"]) > 0:
        return result["response"][0]
    else:
        raise VkApiException(result["error"]["error_msg"])


def get_vk_poll(owner_id, poll_id):
    r = requests.get('https://api.vk.com/method/polls.getById', {
        'access_token': os.getenv('WNM_VK_TOKEN', ''),
        'poll_id': poll_id,
        'owner_id': owner_id,
        'version': 5.87
    })
    result = r.json()

    if 'response' in result:
        return result["response"]
    else:
        raise VkApiException(result["error"]["error_msg"])


def get_last_record(group_id, post_id):
    return (Posts
            .select()
            .where(Posts.group_id == group_id, Posts.post_id == post_id)
            .order_by(Posts.date_update.desc())
            .first())


def update(group_id, post_id):
    data = get_vk_post(group_id, post_id)

    votes = 0
    views = 0
    if 'attachments' in data:
        for attch in data["attachments"]:
            if attch["type"] == "video":
                views = attch["video"]["views"]
            if attch["type"] == 'poll':
                votes = get_vk_poll(data["copy_owner_id"] or data["from_id"], attch["poll"]["poll_id"])["votes"]
    return Posts.create(
        date_update=datetime.now(),
        group_id=group_id,
        post_id=post_id,

        date_create=datetime.fromtimestamp(data["date"]),
        views=views,
        likes=data["likes"]["count"],
        reposts=data["reposts"]["count"],
        comments=data["comments"]["count"],
        votes=votes,
    )


app = Flask(__name__)


@app.route('/', methods=['GET'])
def main():
    item = request.args.get('item')
    if not item:
        return jsonify({'error': 'Not have input param'}), 400
    if not item.startswith('wall'):
        return jsonify({'error': 'Not valid input param'}), 400

    try:
        group_id, post_id = request.args.get('item')[4:].split('_')
        group_id = int(group_id)
        post_id = int(post_id)
    except ValueError:
        return jsonify({'error': 'Not valid input param'}), 400

    last_record = get_last_record(group_id, post_id)
    if (
        last_record is None or
        (datetime.now() - last_record.date_create) < timedelta(minutes=15) or
        (datetime.now() - last_record.date_update) > timedelta(minutes=10)
    ):
        try:
            last_record = update(group_id, post_id)
        except VkApiException:
            return jsonify({'error': 'error in vk api'}), 502
    return jsonify({'post': model_to_dict(last_record)}), 200


@app.route('/history', methods=['GET'])
def history():
    item = request.args.get('item')
    if not item:
        return jsonify({'error': 'Not have input param'}), 400
    if not item.startswith('wall'):
        return jsonify({'error': 'Not valid input param'}), 400

    try:
        group_id, post_id = request.args.get('item')[4:].split('_')
        group_id = int(group_id)
        post_id = int(post_id)
    except ValueError:
        return jsonify({'error': 'Not valid input param'}), 400
    records = [{
        "comments": item.comments,
        "date_create": item.date_create,
        "date_update": item.date_update,
        "group_id": item.group_id,
        "id": item.id,
        "likes": item.likes,
        "post_id": item.post_id,
        "reposts": item.reposts,
        "views": item.views,
        "votes": item.votes
    } for item in Posts
               .select()
               .where(Posts.group_id == group_id, Posts.post_id == post_id)
               .order_by(Posts.date_update.desc())
               .execute()]
    return jsonify({'post': records}), 200


if __name__ == '__main__':
    app.run(debug=True)
